#ifndef SHEDULER_H
#define SHEDULER_H

#include "sfn_shader.h"

namespace r600 {


Shader *schedule(Shader *original);

}

#endif // SHEDULER_H
